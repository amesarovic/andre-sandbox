
# TestNG SQL showcase

Showcase some useful TestNG capabilities for SQL testing.

* [sql-testng-simple](sql-testng-simple/README.md) - Demonstrates basic TestNG DataProvider features for SQL query testing. 
* sql-testng-advanced - TBD


