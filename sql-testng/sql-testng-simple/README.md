
# Simple TestNG SQL showcase

Demonstrates basic TestNG DataProvider features for SQL query testing. 

See 
[DataProviderTest.java](src/test/java/com/amm/sqltest/DataProviderTest.java) and 
[FileDataProviderTest.java](src/test/java/com/amm/sqltest/FileDataProviderTest.java).

Supported database providers:

* PostgreSQL. Default JDBC URL is: jdbc:postgresql://127.0.0.1:5432/sqltest.
* Hadapt. Default URL is: jdbc:hadapt://172.16.1.10:10000/default.

## Install and Run

You can build the project and run the tests using maven or gradle. 
Full functionality is supported by maven. 

There are two properties you can toggle at the command line:

* cfg.sqlTestFile - File containing SQL queries to execute. Assumed directory is [src/test/resources/queries](src/test/resources/queries). Default file is [sample.sql](src/test/resources/queries/sample.sql).
* database.provider - Database provider - either postgresql (default) or hadapt.

### Database Creation

The two basic TPC-H tables <i>nation\_ext</i> and <i>region\_ext</i> are used.

#### PostgreSQL

The default database provider is PostgreSQL. 
You will first need to create a database called <i>sqltest</i>. Then execute the DDL statements in:

* [conf/postgresql/create.sql](conf/postgresql/create.sql)
* [conf/insert-region.sql](conf/insert-region.sql) and [conf/insert-nation.sql](conf/insert-nation.sql)

#### Hadapt

It is assumed you have already created the tables:

	hfab vagrant data.tpch.create_table_ext:table=nation,hdfs_path=/tmphdfs/tpch/nation
	hfab vagrant data.tpch.create_table_ext:table=region,hdfs_path=/tmphdfs/tpch/region

### Maven

After running, the HTML report can be found at: [target/surefire-reports/html/index.html](target/surefire-reports/html/index.html).

	mvn -Dcfg.sqlTestFile=JustSubq.sql test
	mvn -Ddatabase.provider=hadapt test
	mvn -Ddatabase.provider=postgresql test
	mvn -Ddatabase.provider=hadapt -Dcfg.sqlTestFile=JustSubq.sql test

### Gradle

After running, the HTML report can be found at: [build/reports/tests/index.html](build/reports/tests/index.html).
Since I'm new to gradle, default run works but passing system properties via -P doesn't seem to work - grrr... Need some cycles to fix.

	gradle test
	gradle -Pcfg.sqlTestFile=JustSubq.sql test
	gradle -Pdatabase.provider=hadapt test
	gradle -Pdatabase.provider=postgresql test
	gradle -Pdatabase.provider=hadapt -Dcfg.sqlTestFile=JustSubq.sql test

## Configuration
* [testng.xml](src/test/resources/testng/testng.xml) - Standard TestNG file to specify which tests to run
* [src/test/resources](src/test/resources) - Spring configuration 
  * [appContext.xml](src/test/resources/appContext.xml) - Spring bean configuration
  * [appContext.properties](src/test/resources/appContext.properties) - Common properties
  * [appContext-hadapt.properties](src/test/resources/appContext-hadapt.properties) - Hadapt JDBC properties
  * [appContext-postgresql.properties](src/test/resources/appContext-postgresql.properties) - Postgresql JDBC properties
* [src/test/resources/queries](src/test/resources/queries) - SQL test query files

## Sample Files

### Java Files
 * [DataProviderTest.java](src/test/java/com/amm/sqltest/DataProviderTest.java) - Simplest example of DataProvider
 * [FileDataProviderTest.java](src/test/java/com/amm/sqltest/FileDataProviderTest.java) - Populate DataProvider from file

### SQL data files
* [sample.sql](src/test/resources/queries/sample.sql) - simple sample file
* [JustSubq.sql](src/test/resources/queries/JustSubq.sql) - Sub Query test file

## TestNG DataProvider Resources
  * [Parameters with DataProviders](http://testng.org/doc/documentation-main.html#parameters-dataproviders) - TestNG documentation
  * [TestNG Tutorial 6 – Parameterized Test](http://www.mkyong.com/unittest/testng-tutorial-6-parameterized-test/) - mykong - May 2009
  * [TestNG - Parameterized Test](http://www.tutorialspoint.com/testng/testng_parameterized_test.htm) - tutorialspoing 
