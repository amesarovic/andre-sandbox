
drop table if exists region_ext cascade ;
create table region_ext (
    r_regionkey int primary key not null,
    r_name varchar(25) not null unique,
    r_comment varchar(152)
);

drop table if exists nation_ext cascade ;
create table nation_ext (
    n_nationkey int primary key not null,
    n_name varchar(25) not null unique,
    n_regionkey int not null references region (r_regionkey),
    n_comment varchar(152)
);

