package com.amm.sqltest;

import java.util.*;
import java.io.*;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;
import java.nio.charset.Charset;
import com.google.common.io.Files;

public class FileDataProviderTest extends BaseTest {
	private final String dirName = "src/test/resources/queries" ;
	private final static Logger logger = Logger.getLogger(FileDataProviderTest.class);

	@DataProvider(name = "queries")
	public Object[][] queries() throws IOException {
		String fileName = testConfig.getSqlTestFile();
		logger.info("sqlTestFile="+fileName);
		List<String> queries = Files.readLines(new File(dirName,fileName), Charset.defaultCharset());

		Object[][] array = new Object[queries.size()][1];
		int j=0;
		for (String query : queries) {
			array[j++][0] = query;
		}
		return array;
	}

	@Test(dataProvider = "queries")
	public void testQuery(String query) throws Exception {
		sqlExecutor.execute(query);
	}
}
