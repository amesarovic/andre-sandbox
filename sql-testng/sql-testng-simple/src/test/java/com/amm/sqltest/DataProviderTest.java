package com.amm.sqltest;

import org.testng.annotations.*;

public class DataProviderTest extends BaseTest {

	private String [] queries = {
		"select * from nation_ext where n_nationkey < 3",
		"select * from region_ext"
	} ;

	@DataProvider(name = "queries")
	 public Object[][] queries() {
		Object[][] array = new Object[queries.length][1];
		int j=0;
		for (String query : queries) {
			array[j++][0] = query;
		}
		return array;
	}

	@Test(dataProvider = "queries")
	public void testQuery(String query) throws Exception {
		sqlExecutor.execute(query);
	}
}
