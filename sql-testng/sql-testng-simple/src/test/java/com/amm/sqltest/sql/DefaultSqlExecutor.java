package com.amm.sqltest.sql;

import java.util.*;
import java.io.*;
import java.sql.*;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

public class DefaultSqlExecutor implements SqlExecutor {
	private DataSource dataSource ;

	public DefaultSqlExecutor(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void execute(String query) throws Exception {
		Connection conn = dataSource.getConnection();
		long time1 = System.currentTimeMillis();
		Statement stmt = conn.createStatement() ;
		ResultSet rs = stmt.executeQuery(query);
		ResultSetMetaData meta = rs.getMetaData();
		long time = System.currentTimeMillis()-time1;
		int ncols = meta.getColumnCount();

		System.out.println("===============================");
		System.out.println("Time="+time+" Query="+query);
		for (int r=0 ; rs.next() ; r++) {
			for (int c=0 ; c < ncols ; c++) {
				String value  = rs.getString(c+1);
				System.out.print("|"+value);
			}
		System.out.println();
		}
	}
}
