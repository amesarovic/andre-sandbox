package com.amm.sqltest;

import java.util.*;
import org.apache.log4j.Logger;
import org.testng.annotations.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.amm.sqltest.sql.SqlExecutor;

public class BaseTest {
	private final static Logger logger = Logger.getLogger(BaseTest.class);
	static TestConfig testConfig ;
	static SqlExecutor sqlExecutor;

	@BeforeSuite
	public void beforeSuite() {
		initSpring();
	}

	private void initSpring() {
		ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
		testConfig = context.getBean("testConfig",TestConfig.class);
		sqlExecutor = context.getBean("sqlExecutor",SqlExecutor.class);
		logger.info("testConfig="+testConfig);
		logger.info("sqlExecutor="+sqlExecutor);
	}
}
