package com.amm.sqltest;

import java.util.*;

public class TestConfig { 

	private String sqlTestFile; 
	public String getSqlTestFile() { return sqlTestFile; }
	public void setSqlTestFile(String sqlTestFile) { this.sqlTestFile = sqlTestFile; }
 
	@Override
	public String toString() {
		return
			"sqlTestFile=" + sqlTestFile 
		;
	}
}
