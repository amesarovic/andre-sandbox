package com.amm.sqltest.sql;

public interface SqlExecutor {
	public void execute(String query) throws Exception ;
}
